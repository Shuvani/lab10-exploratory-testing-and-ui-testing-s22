describe('Solution III', () => {
  it('Visit page', () => {
    cy.visit('https://www.limeroad.com/')
    cy.get('div').contains('India\'s most loved')
    cy.get('#shopWomen').click()
  })

  it('Click search', () => {
    cy.get('div').contains('search').should('exist')
    cy.get('#deskSearch', { timeout: 30000 }).click()
  })

  it('type bag', () => {
    cy.get('#srcInpu').type('bag').type('{enter}')
  })

  it('Check title', () => {
    cy.get('div').contains('bag').should('exist')
  })
})